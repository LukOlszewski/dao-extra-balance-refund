#!/usr/bin/env python
# encoding: utf-8

"""
@author:     Lukasz Olszewski
@copyright:  N/A
@license:    GNU GPL License
@contact:    email@lukaszolszewski.info
@version:    0.1 - 29/Aug/2016
"""

import os.path
import traceback
import datetime
import re
import sys
import time
import yaml
import calendar
import argparse
import getpass
import pandas
from subprocess import PIPE, Popen
from decimal import Decimal
from threading import Thread
from collections import OrderedDict
try:
    from Queue import Queue, Empty
except ImportError:
    from queue import Queue, Empty  # python 3.x
import getch

ON_POSIX = 'posix' in sys.builtin_module_names

# First few constants are defined.
curator_contract_ABI = '[{"constant":false,"inputs":[{"name":"_owner","type":"address"}],"name":"removeOwner","outputs":[],"type":"function"},{"constant":false,"inputs":[{"name":"_addr","type":"address"}],"name":"isOwner","outputs":[{"name":"","type":"bool"}],"type":"function"},{"constant":true,"inputs":[],"name":"m_numOwners","outputs":[{"name":"","type":"uint256"}],"type":"function"},{"constant":false,"inputs":[],"name":"resetSpentToday","outputs":[],"type":"function"},{"constant":false,"inputs":[{"name":"_owner","type":"address"}],"name":"addOwner","outputs":[],"type":"function"},{"constant":true,"inputs":[],"name":"m_required","outputs":[{"name":"","type":"uint256"}],"type":"function"},{"constant":false,"inputs":[{"name":"_h","type":"bytes32"}],"name":"confirm","outputs":[{"name":"","type":"bool"}],"type":"function"},{"constant":false,"inputs":[{"name":"_newLimit","type":"uint256"}],"name":"setDailyLimit","outputs":[],"type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"},{"name":"_data","type":"bytes"}],"name":"execute","outputs":[{"name":"_r","type":"bytes32"}],"type":"function"},{"constant":false,"inputs":[{"name":"_operation","type":"bytes32"}],"name":"revoke","outputs":[],"type":"function"},{"constant":false,"inputs":[{"name":"_newRequired","type":"uint256"}],"name":"changeRequirement","outputs":[],"type":"function"},{"constant":true,"inputs":[{"name":"_operation","type":"bytes32"},{"name":"_owner","type":"address"}],"name":"hasConfirmed","outputs":[{"name":"","type":"bool"}],"type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"}],"name":"kill","outputs":[],"type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"}],"name":"changeOwner","outputs":[],"type":"function"},{"constant":true,"inputs":[],"name":"m_dailyLimit","outputs":[{"name":"","type":"uint256"}],"type":"function"},{"inputs":[{"name":"_owners","type":"address[]"},{"name":"_required","type":"uint256"},{"name":"_daylimit","type":"uint256"}],"type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"owner","type":"address"},{"indexed":false,"name":"operation","type":"bytes32"}],"name":"Confirmation","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"owner","type":"address"},{"indexed":false,"name":"operation","type":"bytes32"}],"name":"Revoke","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"oldOwner","type":"address"},{"indexed":false,"name":"newOwner","type":"address"}],"name":"OwnerChanged","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"newOwner","type":"address"}],"name":"OwnerAdded","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"oldOwner","type":"address"}],"name":"OwnerRemoved","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"newRequirement","type":"uint256"}],"name":"RequirementChanged","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"from","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Deposit","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"owner","type":"address"},{"indexed":false,"name":"value","type":"uint256"},{"indexed":false,"name":"to","type":"address"},{"indexed":false,"name":"data","type":"bytes"}],"name":"SingleTransact","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"owner","type":"address"},{"indexed":false,"name":"operation","type":"bytes32"},{"indexed":false,"name":"value","type":"uint256"},{"indexed":false,"name":"to","type":"address"},{"indexed":false,"name":"data","type":"bytes"}],"name":"MultiTransact","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"operation","type":"bytes32"},{"indexed":false,"name":"initiator","type":"address"},{"indexed":false,"name":"value","type":"uint256"},{"indexed":false,"name":"to","type":"address"},{"indexed":false,"name":"data","type":"bytes"}],"name":"ConfirmationNeeded","type":"event"}]'

geth_path = 'C:\Program Files\Geth\geth.exe'

contract_name = 'our_contr'
amount_of_gas_per_transaction = 1000000
how_often_to_unlock_geth_account = 3600
how_long_to_wait_for_unlock = 3
how_long_to_wait_before_re_checking_transactions = 300  # 5 minutes, after submitting if after this time the network doesn't provide feedback our transactions were processed correctly
#                                                         the script will check a receipt for each transaction to see if it has been mined. if it hasn't it can be reinserted after the
#                                                         reinsertion time specified below or it can be simply marked to be included in the next run.
how_long_to_wait_before_re_inserting_transactions = 1800  # half an hour, this is the time to wait before re-inserting non-mined transactions, if you prefer not to re-insert, change to 0
# The below hashes to recognise events are calculated by geth by running the following
# web3.sha3("Confirmation(address,bytes32)")
# web3.sha3("ConfirmationNeeded(bytes32,address,uint256,address,bytes)")
# web3.sha3("MultiTransact(address,bytes32,uint256,address,bytes)")
# web3.sha3("Revoke(address,bytes32)")
events_decode = dict(Confirmation="0xe1c52dc63b719ade82e8bea94cc41a0d5d28e4aaf536adb5e9cccc9ff8c1aeda",
                     ConfirmationNeeded="0x1733cbb53659d713b79580f79f3f9ff215f78a7c7aa45890f3b89fc5cddfbf32",
                     MultiTransact="0xe7c957c06e9a662c1a6c77366179f5b702b97651dc28eee7d5bf1dff6e40bb4a",
                     Revoke="0xc7fb647e59b18047309aa15aad418e5d7ca96d173ad704f1031a2c3d7591734b")
events_decode_types = dict(Confirmation=['address', 'bytes32'],
                           ConfirmationNeeded=['bytes32', 'address', 'uint256', 'address', 'bytes'],
                           MultiTransact=['address', 'bytes32', 'uint256', 'address', 'bytes'],
                           Revoke=['address', 'bytes32'])
events_decode_fields = dict(Confirmation=[('operation', 1), ('owner', 0)],
                            ConfirmationNeeded=[('data', 4), ('initiator', 1), ('operation', 0), ('to', 3), ('value', 2)],
                            MultiTransact=[('data', 4), ('operation', 1), ('owner', 0), ('to', 3), ('value', 2)],
                            Revoke=[('operation', 1), ('owner', 0)])

# processing command line arguments
parser = argparse.ArgumentParser()
parser.add_argument("Contract_Address", type=str, help="Wallet Contract base address, default is 0xDa4a4626d3E16e094De3225A751aAb7128e96526", default='0xDa4a4626d3E16e094De3225A751aAb7128e96526')
parser.add_argument("FromAddress", type=str, help="Your address we're sending the confirmation from.")
parser.add_argument("InputDataFile", type=str, help="The file data for refunds is taken from.")
parser.add_argument("OutputDataFile", type=str, help="The file results of this operation will be saved to.")
parser.add_argument("OutputLogFile", type=str, help="The file log will be saved to.")
parser.add_argument("OutputDoneFile", type=str, help="The file will contain details of accounts the refund was successfull for, it is updated in real time.")
parser.add_argument("mode", type=str, help="The mode of operation, one of inserting - submitting new transations, confirming - confirming transactions as a second Wallet curator, revoking - removing transactions submitted in error or during testing, listening - mode created to record late events.")
parser.add_argument("-d", "--debug", action="count", default=0, help="enable debug, enter twice for very detailed debug")
parser.add_argument("-f", "--final", action="store_true", help="Is this the final(third) confirmation run? If yes, the tool will expect MultiTransact events.")
parser.add_argument('--start', type=int, default=0, help="This is the starting row number we read from the data file.")
parser.add_argument('--end', type=int, default=99999999999, help="This is the last row number we read from the data file.")
args = parser.parse_args()

if args.Contract_Address and len(args.Contract_Address) == 42:
    curator_contract_address = str(args.Contract_Address).lower()
else:
    print ("Please supply a valid curator contract address")
    exit(0)

if args.FromAddress and len(args.FromAddress) == 42:
    from_address = str(args.FromAddress).lower()
else:
    print ("Please supply a valid curator contract address")
    exit(0)

refund_targets_file_name = args.InputDataFile
processed_targets_file_name = args.OutputDataFile
log_file_name = args.OutputLogFile
done_targets_file_name = args.OutputDoneFile
if not args.mode or args.mode not in ['inserting', 'confirming', 'revoking', 'listening']:
    print("Please supply a valid mode: inserting, listening, confirming or revoking")
else:
    mode = args.mode

if args.debug == 0:
    debug = False
    geth_output_debug = False
elif args.debug == 1:
    debug = True
    geth_output_debug = False
elif args.debug == 2:
    debug = True
    geth_output_debug = True

if args.final:
    final_run = True
    print ("This run expects to run the final confirmation.")
    if mode != 'confirming':
        print ('The mode for the final run should be "confirming"')
else:
    final_run = False
    if mode == 'inserting':
        print ('This is the first "inserting" run.')
    elif mode == 'confirming':
        print ('This is the confirmation run.')
    elif mode == 'revoking':
        print ('This is a revoking run.')

print ('\nStarting up in "' + mode + '" mode, will send all commands to a curator contract at "' + curator_contract_address + '" from address "' + from_address + '" taking data from file named "' + refund_targets_file_name + '" and saving final result into "' + processed_targets_file_name + '", saving logs live to "' + log_file_name + '", with a completion file of "' + done_targets_file_name + '"\n')

if mode != 'listening':
    password = getpass.getpass("Please provide a password to unlock the relevant geth account:")

# helper variables
targets_to_do = pandas.DataFrame()
log_writer_thread = None
status_manager_thread = None
user_interrupt = None
status_manager_queue_delayer = None
last_time_account_unlocked = None
output_queue = None
delta_events_in_this_re_checking_period = 0
all_geth_output_responses = Queue()


# Sometimes events from a wallet contract arrive out of order, this means we have to delay some events to change their order, this thread does that.
class StatusManagerQueueDelayer(Thread):
    def __init__(self, **kwargs):
        super(StatusManagerQueueDelayer, self).__init__(**kwargs)
        self.delay_q = Queue()
        self.delay = 3

    def run(self):
        global status_manager_thread
        while True:
            item = self.delay_q.get()
            time.sleep(self.delay)
            status_manager_thread.event_queue.put(item)


class StatusManager(Thread):
    def __init__(self, **kwargs):
        super(StatusManager, self).__init__(**kwargs)
        self.event_queue = Queue()
        self.waiting_for = pandas.DataFrame()
        self.allDone = False
        self.started = False
        self.noMoreNewTransactions = False

    def run(self):
        global targets_to_do
        global log_writer_thread
        global debug
        global geth_output_debug
        global from_address
        global status_manager_queue_delayer
        global delta_events_in_this_re_checking_period

        fail_re = re.compile('^[> ]*Transaction ([0-9xa-fA-F]+) failed, result:(.*)')
        succeed_re = re.compile('^[> ]*Transaction ([0-9xa-fA-F]+) submitted with id:([0-9xa-fA-F]+)')
        confirmation_re = re.compile('^[> ]*Confirmation: {"operation":"([0-9xa-fA-F]+)","owner":"([0-9xa-fA-F]+)"}')
        confirmation_needed_re = re.compile('^[> ]*ConfirmationNeeded: {"data":"0x","initiator":"([0-9xa-fA-F]+)","operation":"([0-9xa-fA-F]+)","to":"([0-9xa-fA-F]+)","value":"([0-9]+)"}')
        multitransact_re = re.compile('^[> ]*MultiTransact: {"data":"0x","operation":"([0-9xa-fA-F]+)","owner":"([0-9xa-fA-F]+)","to":"([0-9xa-fA-F]+)","value":"([0-9]+)"}')
        revoke_re = re.compile('^[> ]*Revoke: {"operation":"([0-9xa-fA-F]+)","owner":"([0-9xa-fA-F]+)"}')
        while not self.allDone:
            try:
                if len(self.waiting_for) > 0 and not self.started:
                    self.started = True
                if self.noMoreNewTransactions and self.started and len(self.waiting_for) == 0:
                    self.allDone = True
                output_line = self.event_queue.get()
                if "failed" in output_line and fail_re.match(output_line):
                    match = fail_re.match(output_line)
                    failed_hash = match.group(1)
                    error_msg = match.group(2)
                    self.waiting_for = self.waiting_for.query('TxHash != "'+failed_hash+'"').copy()
                    log_writer_thread.log_output.put("Transaction with hash "+failed_hash+' failed due to error:'+error_msg)
                elif 'submitted with id' in output_line and succeed_re.match(output_line):
                    match = succeed_re.match(output_line)
                    succeed_hash = match.group(1)
                    trans_id = match.group(2)
                    set_transaction_submitted(succeed_hash)
                    set_transaction_submitted_at(succeed_hash)
                    set_transaction_submission_id(succeed_hash, trans_id)
                    log_writer_thread.log_output.put("Transaction with hash " + succeed_hash + ' was submitted with an id of:' + trans_id)
                    self.waiting_for = self.waiting_for.append(targets_to_do.loc[succeed_hash])
                elif "Confirmation:" in output_line and confirmation_re.match(output_line):
                    match = confirmation_re.match(output_line)
                    operation_hash = match.group(1)
                    owner = match.group(2)
                    if owner != from_address:
                        log_writer_thread.log_output.put("Someone else's confirmation received: " + operation_hash + ' with an owner:' + owner + '. Skipping it')
                        continue
                    lookup = targets_to_do.query('WalletTransactionHash == "'+operation_hash+'"')
                    if len(lookup) != 1:
                        log_writer_thread.log_output.put("Received our confirmation, but most likely out of turn, the operation hash is: " + operation_hash + '. Puttin it in a 3s delay loop.')
                        status_manager_queue_delayer.delay_q.put(output_line)
                        continue
                    tx_hash = lookup.index[0]
                    set_confirmed_by(tx_hash, operation_hash)
                    if not final_run:
                        self.waiting_for = self.waiting_for.query('TxHash != "' + tx_hash + '"').copy()
                    delta_events_in_this_re_checking_period += 1
                elif "ConfirmationNeeded:" in output_line and confirmation_needed_re.match(output_line):
                    match = confirmation_needed_re.match(output_line)
                    owner = match.group(1)
                    operation_hash = match.group(2)
                    destination_addr = match.group(3)
                    value = long(Decimal(match.group(4)))
                    if owner != from_address:
                        log_writer_thread.log_output.put("Someone else's confirmation needed event received: " + operation_hash + ' with an owner:' + owner + ' destination of:'+destination_addr+' and value of:' + str(value) + '. Skipping it')
                        continue
                    lookup = targets_to_do.query('TokenOwner == "'+destination_addr+'" and extraBalanceAmount == '+str(value))
                    if len(lookup) != 1:
                        log_writer_thread.log_output.put("Received confirmation needed with non matching data,activity hash present: " + operation_hash + ' owner is:' + owner + '. Skipping it')
                        continue
                    tx_hash = lookup.index[0]
                    log_writer_thread.log_output.put("Confirmation of our operation needed " + str(operation_hash) + ' was submitted with an id of:' + str(tx_hash))
                    if len(targets_to_do.get_value(tx_hash, 'WalletTransactionHash')) == 0:
                        targets_to_do.set_value(tx_hash, 'WalletTransactionHash', operation_hash)
                    else:
                        log_writer_thread.log_output.put("Hash was going to be put in, but WalletTransactionHash field not empty. Skipping transaction" + str(tx_hash))
                        old_hash = targets_to_do.get_value(tx_hash, 'WalletTransactionHash')
                        log_writer_thread.log_output.put("Our hash" + str(tx_hash) + ' the existing hash (better they are the same):' + str(old_hash))
                    delta_events_in_this_re_checking_period += 1
                elif "MultiTransact:" in output_line and multitransact_re.match(output_line):
                    match = multitransact_re.match(output_line)
                    operation_hash = match.group(1)
                    owner = match.group(2)
                    destination_addr = match.group(3)
                    value = long(Decimal(match.group(4)))
                    lookup = targets_to_do.query('WalletTransactionHash == "'+operation_hash+'" and extraBalanceAmount == '+str(value)+' and TokenOwner == "'+destination_addr+'"')
                    if len(lookup) != 1:
                        log_writer_thread.log_output.put("Received multi transfer event, but it is not one of ours it has: " + operation_hash + ', value of:'+str(value)+' and it is for:'+destination_addr+', it was sent from:'+owner+' Skipping it')
                        continue
                    tx_hash = lookup.index[0]
                    log_writer_thread.log_output.put("Multi transaction confirmed hash:" + operation_hash + ' was submitted with an id of:' + str(tx_hash) + ' for value:'+str(value)+', to:'+destination_addr)
                    self.waiting_for = self.waiting_for.query('TxHash != "' + tx_hash + '"').copy()
                    log_writer_thread.log_output.put(targets_to_do[tx_hash].copy())
                    delta_events_in_this_re_checking_period += 1
                elif "Revoke:" in output_line and revoke_re.match(output_line):
                    log_writer_thread.log_output.put("Revoke event received:"+output_line)
                    match = revoke_re.match(output_line)
                    operation_hash = match.group(1)
                    owner = match.group(2)
                    lookup = targets_to_do.query('WalletTransactionHash == "'+operation_hash+'"')
                    if len(lookup) != 1:
                        log_writer_thread.log_output.put("Received revoke event, but it is not one of ours it has: " + operation_hash + ', it was sent from:'+owner+' Skipping it')
                        continue
                    tx_hash = lookup.index[0]
                    log_writer_thread.log_output.put("Transaction revoked, its hash:" + operation_hash + ' was submitted with an id of:' + str(tx_hash) + ' for value:'+str(value)+', to:'+destination_addr)
                    self.waiting_for = self.waiting_for.query('TxHash != "' + tx_hash + '"').copy()
                    targets_to_do.set_value(tx_hash, 'TransactionRevoked', 'Yes')
                    delta_events_in_this_re_checking_period += 1
            except Exception as e:
                log_writer_thread.log_output.put("Parser crash detected with data:"+output_line+" reinjecting the line with 3s delay.")
                exc_type, exc_value, exc_traceback = sys.exc_info()
                log_writer_thread.log_output.put("Error message is:" + e.message + ', in line:' + str(exc_traceback.tb_lineno))
                status_manager_queue_delayer.delay_q.put(output_line)
                continue


def set_confirmed_by(txhash, op_hash):
    global targets_to_do
    global log_writer_thread
    try:
        if mode == 'inserting':
            targets_to_do.set_value(txhash, 'Confirmation1by', from_address)
        elif mode == 'confirming' and not args.final:
            targets_to_do.set_value(txhash, 'Confirmation2by', from_address)
        elif mode == 'confirming' and args.final:
            targets_to_do.set_value(txhash, 'Confirmation3by', from_address)
        log_writer_thread.log_output.put("Confirmation of our operation " + str(op_hash) + ' was submitted with an id of:' + str(txhash))
    except Exception as e:
        log_writer_thread.log_output.put("Submission of operation " + str(op_hash) + ' with an id of:' + str(txhash) + ' failed with message:'+e.message)


def set_transaction_submitted(txhash):
    global targets_to_do
    global log_writer_thread
    global mode
    global args
    try:
        if mode == 'inserting':
            targets_to_do.set_value(txhash, 'TransactionSubmitted1', 'Yes')
        elif mode == 'confirming' and not args.final:
            targets_to_do.set_value(txhash, 'TransactionSubmitted2', 'Yes')
        elif mode == 'confirming' and args.final:
            targets_to_do.set_value(txhash, 'TransactionSubmitted3', 'Yes')
        log_writer_thread.log_output.put("Transaction submission saved for  " + str(txhash))
    except Exception as e:
        log_writer_thread.log_output.put('Transaction submission failed with message:'+e.message)


def set_transaction_submitted_at(txhash):
    global targets_to_do
    global log_writer_thread
    global mode
    global args
    try:
        if mode == 'inserting' or mode == 'revoking':
            targets_to_do.set_value(txhash, 'TransactionSubmittedAt1', int(calendar.timegm(time.gmtime())))
        elif mode == 'confirming' and not args.final:
            targets_to_do.set_value(txhash, 'TransactionSubmittedAt2', int(calendar.timegm(time.gmtime())))
        elif mode == 'confirming' and args.final:
            targets_to_do.set_value(txhash, 'TransactionSubmittedAt3', int(calendar.timegm(time.gmtime())))
        log_writer_thread.log_output.put("Transaction submission time saved for  " + str(txhash))
    except Exception as e:
        log_writer_thread.log_output.put('Transaction submission time failed with message:'+e.message)


def set_transaction_submission_id(txhash, id):
    global targets_to_do
    global log_writer_thread
    global mode
    global args
    try:
        if mode == 'inserting':
            targets_to_do.set_value(txhash, 'TransactionSubmissionId1', id)
        elif mode == 'confirming' and not args.final:
            targets_to_do.set_value(txhash, 'TransactionSubmissionId2', id)
        elif mode == 'confirming' and args.final:
            targets_to_do.set_value(txhash, 'TransactionSubmissionId3', id)
        log_writer_thread.log_output.put("Transaction submission id saved for  " + str(txhash) + ' id is:' + str(id))
    except Exception as e:
        log_writer_thread.log_output.put('Transaction submission id failed with message:'+e.message)


class DoneWriter(Thread):
    def __init__(self, **kwargs):
        super(DoneWriter, self).__init__(**kwargs)
        self.allDone = False
        self.doneQueue = Queue()

    def run(self):
        while not self.allDone:
            row = self.doneQueue.get()
            if os.path.isfile(done_targets_file_name):
                row.to_csv(done_targets_file_name, sep='\t', header=False, mode='w')
            else:
                row.to_csv(done_targets_file_name, sep='\t', header=True, mode='a')


class LogWriter(Thread):
    def __init__(self, **kwargs):
        super(LogWriter, self).__init__(**kwargs)
        self.allDone = False
        self.log_output = Queue()

    def run(self):
        global debug
        global geth_output_debug
        global log_file_name
        while not self.allDone:
                if debug:
                    try:
                        line = self.log_output.get(timeout=2)
                        with open(log_file_name, 'a') as logfile_handle:
                            logfile_handle.write("L:" + line+'\n')
                            print ("L:"+line+'\n')
                    except:
                        pass


class UserInterrupt(Thread):
    def __init__(self, **kwargs):
        super(UserInterrupt, self).__init__(**kwargs)
        self.interrupted = False

    def run(self):
        while not self.interrupted:
            keypress = getch.getch()
            if keypress == 'q':
                self.interrupted = True


class OutputQueue(Thread):
    def __init__(self,out, dummy, **kwargs):
        super(OutputQueue, self).__init__(**kwargs)
        self.out = out
        self.redirect_output = False
        self.redirect_output_to = None
        self.tempQueue = Queue()

    def run(self):
        global log_writer_thread
        global status_manager_thread
        global debug
        global geth_output_debug
        global all_geth_output_responses
        independent_line_re = re.compile('^[> ]?[a-zA-Z]+: {.*}')

        for line in iter(self.out.readline, b''):
            if not self.redirect_output:
                if "Confirmation:" in line or "ConfirmationNeeded:" in line or 'failed' in line or 'submitted with id' in line or "MultiTransact:" in line:
                    status_manager_thread.event_queue.put(line)
            else:
                self.redirect_output_to.put(line)
                if independent_line_re.match(line):
                    self.tempQueue.put(line)
            if geth_output_debug:
                log_writer_thread.log_output.put(line)
            all_geth_output_responses.put(line)
        self.out.close()

    def set_redirect_output(self, where):
        self.redirect_output_to = where
        self.redirect_output = True

    def cancel_output_redirection(self):
        if not self.redirect_output:
            return
        self.redirect_output = False
        done = False
        while not done:
            try:
                line = self.tempQueue.get(timeout=0.1)
                if "Confirmation:" in line or "ConfirmationNeeded:" in line or 'failed' in line or 'submitted with id' in line or "MultiTransact:" in line:
                    status_manager_thread.event_queue.put(line)
            except Empty:
                done = True


def unlock_geth(process):
    global last_time_account_unlocked
    global how_often_to_unlock_geth_account
    global password
    global from_address
    global how_long_to_wait_for_unlock

    if (last_time_account_unlocked is not None and calendar.timegm(time.gmtime()) - last_time_account_unlocked > (how_often_to_unlock_geth_account - 5)) or last_time_account_unlocked is None:
        # unlocking geth account
        code_to_send = 'personal.unlockAccount("'+from_address+'" , "'+password+'", '+str(how_often_to_unlock_geth_account)+')\n'
        process.stdin.write(code_to_send)
        process.stdin.flush()
        last_time_account_unlocked = calendar.timegm(time.gmtime())


def refund(process, single_row=None, work_list=None):
    global contract_name
    global from_address
    global amount_of_gas_per_transaction
    try:
        unlock_geth(process)
        code_to_send = contract_name + '.execute.sendTransaction("' + single_row.iloc[0].get_value('TokenOwner') + '",' + str(single_row.iloc[0].get_value('extraBalanceAmount')) + ',0,{from: "' + from_address + '", gas:' + str(amount_of_gas_per_transaction) + '}, function(error, result){ if(!error) console.log("Transaction ' + single_row.iloc[0].get_value('TxHash') + ' submitted with id:"+result); else console.error("Transaction ' + single_row.iloc[0].get_value('TxHash') + ' failed, result:"+error)});\n'
        work_list.append(single_row)
        process.stdin.write(code_to_send)
        process.stdin.flush()
    except Exception as e:
        log_writer_thread.log_output.put("Inserting one transaction failed. The message is:" + e.message)


def confirm(process, single_row=None, work_list=None):
    global contract_name
    global from_address
    global amount_of_gas_per_transaction
    try:
        if "WalletTransactionHash" not in single_row.columns.tolist():
            return False
        unlock_geth(process)
        code_to_send = contract_name + '.confirm.sendTransaction("' + single_row.iloc[0].get_value('WalletTransactionHash') + '",{from: "' + from_address + '", gas:' + str(amount_of_gas_per_transaction) + '}, function(error, result){ if(!error) console.log("Transaction ' + single_row.iloc[0].get_value('TxHash') + ' submitted with id:"+result); else console.error("Transaction ' + single_row.iloc[0].get_value('TxHash') + ' failed, result:"+error)});\n'
        work_list.append(single_row)
        process.stdin.write(code_to_send)
        process.stdin.flush()
    except Exception as e:
        log_writer_thread.log_output.put("Inserting one transaction failed. The message is:" + e.message)


def revoke(process, single_row=None, work_list=None):
    global contract_name
    global from_address
    global amount_of_gas_per_transaction
    try:
        if "WalletTransactionHash" not in single_row.columns.tolist():
            return False
        unlock_geth(process)
        code_to_send = contract_name + '.revoke.sendTransaction("' + single_row.iloc[0].get_value('WalletTransactionHash') + '",{from: "' + from_address + '", gas:' + str(amount_of_gas_per_transaction) + '}, function(error, result){ if(!error) console.log("Transaction ' + single_row.iloc[0].get_value('TxHash') + ' submitted with id:"+result); else console.error("Transaction ' + single_row.iloc[0].get_value('TxHash') + ' failed, result:"+error)});\n'
        work_list.append(single_row)
        process.stdin.write(code_to_send)
        process.stdin.flush()
    except Exception as e:
        log_writer_thread.log_output.put("Inserting one transaction failed. The message is:" + e.message)


def read_json_output_from_geth(my_geth_output_queue):
    open_response_re = re.compile('^[>]?\{')
    close_response_re = re.compile('^[>]?\}')
    mid_response_re = re.compile('^[ ]+[a-zA-Z]+:')
    done_reading = False
    json_started = False
    started_reading_at = int(calendar.timegm(time.gmtime()))
    # We use 5s timeout, otherwise it would be possible for out main thread to get stuck here
    read_geth_output = ''
    while not done_reading and int(calendar.timegm(time.gmtime())) - started_reading_at < 5:
        try:
            line = my_geth_output_queue.get(timeout=1)
            if not json_started:
                if open_response_re.match(line):
                    json_started = True
                    read_geth_output = '{\n'
                    continue
            else:
                if close_response_re.match(line):
                    read_geth_output += '}\n'
                    done_reading = True
                elif mid_response_re.match(line):
                    read_geth_output += line
                    continue
        except Empty:
            pass
    if not done_reading:
        raise IOError("Geth didn't respond to getTransaction in under 5s.")
    return yaml.load(read_geth_output)


def decode_data(data, dtype):
    if dtype == 'uint256':
        return str(long(data[:64], 16))
    elif dtype == 'address':
        return '0x' + str(data[24:64])
    elif dtype == 'bytes32':
        return '0x' + str(data[:64])
    elif dtype == 'bytes20':
        return '0x' + str(data[:40])
    elif dtype == 'bytes':
        return '0x' + str(data)
    return 0


def decode_event(event_name, event, testing=False):
    global status_manager_thread
    if event_name not in events_decode_types.keys():
        raise IOError("No recognised event sent to decode_event function - please fix the event_tables in lines 43-61.")
    list_of_types_to_process = events_decode_types[event_name]
    # data = map(ord, event['data'][2:].decode('hex'))
    data = event['data'][2:]
    list_of_variables_to_process = sorted(events_decode_fields[event_name], key=lambda entry: entry[1])
    var_values = OrderedDict()
    for i, var in enumerate(list_of_variables_to_process):
        var_values[var[0]] = decode_data(data,  list_of_types_to_process[i])
        if list_of_types_to_process[i] == 'address' or list_of_types_to_process[i] == 'bytes32' or list_of_types_to_process[i] == 'uint256':
            data = data[64:]
        elif list_of_types_to_process[i] == 'bytes20':
            data = data[40:]
        # we skip 'bytes' array because it is always in the end anyway
    our_event = event_name + ': {'
    for k in var_values.keys():
        our_event += '"' + str(k) + '":"' + str(var_values[k]) + '",'
    our_event = our_event[:-1] + '}'
    if not testing:
        status_manager_thread.event_queue.put(our_event)
    else:
        print(our_event)
    return True


def have_transaction_been_mined_and_simulate_lost_event(process, trans_hash, testing=False):
    global output_queue
    my_geth_output_queue = Queue()
    code_to_send = 'eth.getTransaction("'+trans_hash+'");\n'
    output_queue.set_redirect_output(my_geth_output_queue)
    process.stdin.write(code_to_send)
    process.stdin.flush()
    ret = read_json_output_from_geth(my_geth_output_queue)
    if 'blockNumber' not in ret.keys():
        raise BaseException("Geth returned unreadable response to getTransaction!")
    if ret['blockNumber'] is None:
        return False
    code_to_send = 'eth.getTransactionReceipt("' + trans_hash + '");\n'
    process.stdin.write(code_to_send)
    process.stdin.flush()
    ret = read_json_output_from_geth(my_geth_output_queue)
    output_queue.cancel_output_redirection()
    if len(ret['logs']) == 0:
        raise BaseException("Geth returned response with no logs with getTransactionReceipt!")
    for event in ret['logs']:
        for event_name in events_decode.keys():
            if event['topics'][0] == events_decode[event_name]:
                decode_event(event_name, event, testing=testing)
    return True


def recheck_transactions(process):
    global targets_to_do
    global mode
    global args
    if args.start == 0 and args.end == 99999999999:
        transactions_processed = targets_to_do
    elif args.start == 0 and args.end != 99999999999:
        transactions_processed = targets_to_do.iloc[:min(len(targets_to_do), args.end)]
    elif args.start != 0 and args.end == 99999999999:
        transactions_processed = targets_to_do.iloc[args.start, len(targets_to_do):]
    elif args.start != 0 and args.end != 99999999999:
        transactions_processed = targets_to_do.iloc[args.start:min(len(targets_to_do), args.end)]
    else:
        transactions_processed = targets_to_do

    if mode == 'inserting':
        transactions_to_recheck = transactions_processed.query('TransactionSubmittedAt1 <= '+str(int(calendar.timegm(time.gmtime()))-how_long_to_wait_before_re_inserting_transactions)+' and WalletTransactionHash == ""').copy()
    elif mode == 'confirming' and not args.final:
        transactions_to_recheck = transactions_processed.query('TransactionSubmittedAt2 <= ' + str(int(calendar.timegm(time.gmtime())) - how_long_to_wait_before_re_inserting_transactions)).copy()
    elif mode == 'confirming' and args.final:
        transactions_to_recheck = transactions_processed.query('TransactionSubmittedAt3 <= ' + str(int(calendar.timegm(time.gmtime())) - how_long_to_wait_before_re_inserting_transactions)).copy()
    elif mode == 'revoking':
        transactions_to_recheck = transactions_processed.query('TransactionSubmittedAt1 <= ' + str(int(calendar.timegm(time.gmtime())) - how_long_to_wait_before_re_inserting_transactions) + ' and TransactionRevoked == "No"').copy()
    else:
        return
    if len(transactions_to_recheck) == 0:
        print("Recheck run: no transactions to recheck.")
        return
    bad_counter = 0
    good_counter = 0
    print ("Rechecking transactions, a dot will be displayed for each 100")
    for hsh in transactions_to_recheck.index:
        if mode == 'inserting':
            our_hsh = targets_to_do.get_value(hsh, 'TransactionSubmissionId1')
        elif mode == 'confirming' and not args.final:
            our_hsh = targets_to_do.get_value(hsh, 'TransactionSubmissionId2')
        elif mode == 'confirming' and args.final:
            our_hsh = targets_to_do.get_value(hsh, 'TransactionSubmissionId3')
        res = have_transaction_been_mined_and_simulate_lost_event(process, our_hsh)
        if res:
            good_counter += 1
        else:
            bad_counter += 1
        if (good_counter+bad_counter) % 100 == 0:
            sys.stdout.write('.')
            sys.stdout.flush()
            time.sleep(1)  # to allow processing of already sent transactions
    print('Recheck run: ' + good_counter + ' lost events re-created, events for ' + bad_counter + ' transactions not found.')
    return


def re_insert_transactions(process):
    global targets_to_do
    global mode
    global args
    dummy_frame = pandas.DataFrame()
    if args.start == 0 and args.end == 99999999999:
        transactions_processed = targets_to_do
    elif args.start == 0 and args.end != 99999999999:
        transactions_processed = targets_to_do.iloc[:min(len(targets_to_do), args.end)]
    elif args.start != 0 and args.end == 99999999999:
        transactions_processed = targets_to_do.iloc[args.start, len(targets_to_do):]
    elif args.start != 0 and args.end != 99999999999:
        transactions_processed = targets_to_do.iloc[args.start:min(len(targets_to_do), args.end)]
    if mode == 'listening':
        return
    if mode == 'inserting':
        transactions_to_reinsert = transactions_processed.query('TransactionSubmittedAt1 <= '+str(int(calendar.timegm(time.gmtime()))-how_long_to_wait_before_re_inserting_transactions)+' and WalletTransactionHash == ""').copy()
    elif mode == 'confirming' and not args.final:
        transactions_to_reinsert = transactions_processed.query('TransactionSubmittedAt2 <= ' + str(int(calendar.timegm(time.gmtime())) - how_long_to_wait_before_re_inserting_transactions)).copy()
    elif mode == 'confirming' and args.final:
        transactions_to_reinsert = transactions_processed.query('TransactionSubmittedAt3 <= ' + str(int(calendar.timegm(time.gmtime())) - how_long_to_wait_before_re_inserting_transactions)).copy()
    elif mode == 'revoking':
        transactions_to_reinsert = transactions_processed.query('TransactionSubmittedAt1 <= ' + str(int(calendar.timegm(time.gmtime())) - how_long_to_wait_before_re_inserting_transactions) + ' and TransactionRevoked == "No"').copy()
    else:
        return
    if len(transactions_to_reinsert) == 0:
        print("Reinsert run: no transactions to recheck.")
        return
    print ("Reinserting transactions, a dot will be displayed for each 20")
    for counter, hsh in enumerate(transactions_to_reinsert.index):
        if mode == 'inserting':
            funct = 'refund'
        elif mode == 'confirming':
            funct = 'confirm'
        elif mode == 'revoking':
            funct == 'revoke'
        else:
            raise SyntaxError("Mode wrong, it has to be one of inserting, confirming, revoking or listening.")
        globals()[funct](process, single_row=transactions_to_reinsert.iloc[counter:counter + 1], work_list=dummy_frame)
        waiting_for_geth = True
        while waiting_for_geth:
            try:
                line = all_geth_output_responses.get(timeout=5)
                geth_timeouts = 0
                waiting_for_geth = False
            except:
                geth_timeouts += 1
                sys.stdout.write('x')
                sys.stdout.flush()
            if geth_timeouts > 5:
                print ("Geth is stuck.. waiting 30s then trying to insert the next transaction, any transactions it misses will be retried later.")
                time.sleep(30)
                waiting_for_geth = False
        if counter % 20 == 0:
            sys.stdout.write('.')
            sys.stdout.flush()
            time.sleep(1)  # to allow processing of already sent transactions
    print('Reinsert run: ' + counter + ' transactions were reinserted.')
    return


def main(argv):
    try:
        global targets_to_do
        global args
        global log_writer_thread
        global status_manager_thread
        global status_manager_queue_delayer
        global user_interrupt
        global debug
        global mode
        global log_file_name
        global geth_output_debug
        global output_queue
        global delta_events_in_this_re_checking_period

        # First reading a list of already sent refunds
        targets_done = pandas.DataFrame()
        if os.path.isfile(done_targets_file_name):
            with open(done_targets_file_name, "r") as refund_targets_done:
                targets_done = pandas.read_csv(refund_targets_done, sep='\t', index_col=None, parse_dates=False,  converters=dict(extraBalanceAmount=lambda x: long(Decimal(x)*Decimal('1000000000000000000'))))
        # Reading the list of all refunds minus those already sent
        with open(refund_targets_file_name) as refund_targets:
            targets_loaded = pandas.read_csv(refund_targets, sep='\t', index_col=None, parse_dates=False, converters=dict(extraBalanceAmount=lambda x: long(Decimal(x)*Decimal('1000000000000000000'))))
        if len(targets_done) > 0:
            print ("Pleas be patient, removing already done transfers from the list. For every 1 thousand addresses a dot will be displayed.")
            for x in xrange(len(targets_loaded)):
                if len(pandas.merge(targets_done, targets_loaded.iloc[x:x+1], targets_loaded.columns.tolist())) == 0:
                    targets_to_do = targets_to_do.append(targets_loaded.iloc[x:x+1])
                if x % 1000:
                    sys.stdout.write('.')
                    sys.stdout.flush()
            print ("Done, continuing.")
        else:
            targets_to_do = targets_loaded
        targets_to_do['index'] = targets_to_do['TxHash']
        targets_to_do = targets_to_do.set_index('index')
        if 'WalletTransactionHash' not in targets_to_do.columns.tolist():
            targets_to_do['WalletTransactionHash'] = ''
        if 'Confirmation1by' not in targets_to_do.columns.tolist():
            targets_to_do['Confirmation1by'] = ''
        if 'TransactionSubmitted1' not in targets_to_do.columns.tolist():
            targets_to_do['TransactionSubmitted1'] = 'No'
        if 'TransactionSubmittedAt1' not in targets_to_do.columns.tolist():
            targets_to_do['TransactionSubmittedAt1'] = 0
        if 'TransactionSubmissionId1' not in targets_to_do.columns.tolist():
            targets_to_do['TransactionSubmissionId1'] = 0
        if 'Confirmation2by' not in targets_to_do.columns.tolist():
            targets_to_do['Confirmation2by'] = ''
        if 'TransactionSubmitted2' not in targets_to_do.columns.tolist():
            targets_to_do['TransactionSubmitted2'] = 'No'
        if 'TransactionSubmittedAt2' not in targets_to_do.columns.tolist():
            targets_to_do['TransactionSubmittedAt2'] = 0
        if 'TransactionSubmissionId2' not in targets_to_do.columns.tolist():
            targets_to_do['TransactionSubmissionId2'] = 0
        if 'Confirmation3by' not in targets_to_do.columns.tolist():
            targets_to_do['Confirmation3by'] = ''
        if 'TransactionSubmitted3' not in targets_to_do.columns.tolist():
            targets_to_do['TransactionSubmitted3'] = 'No'
        if 'TransactionSubmittedAt3' not in targets_to_do.columns.tolist():
            targets_to_do['TransactionSubmittedAt3'] = 0
        if 'TransactionSubmissionId1' not in targets_to_do.columns.tolist():
            targets_to_do['TransactionSubmissionId3'] = 0
        if 'TransactionRevoked' not in targets_to_do.columns.tolist():
            targets_to_do['TransactionRevoked'] = 'No'

        # Setting up output processing threads
        status_manager_thread = StatusManager()
        done_writer_thread = DoneWriter()
        log_writer_thread = LogWriter()
        status_manager_queue_delayer = StatusManagerQueueDelayer()
        user_interrupt = UserInterrupt()

        # Starting geth, sending its output to a Thread safe Queue called q
        process = Popen([geth_path, 'attach'], stdin=PIPE, stdout=PIPE, bufsize=1, close_fds=ON_POSIX)
        output_queue = OutputQueue(process.stdout, None)
        output_queue.daemon = True
        output_queue.start()

        user_interrupt.daemon = True
        status_manager_queue_delayer.daemon = True
        status_manager_thread.daemon = True
        done_writer_thread.daemon = True
        log_writer_thread.daemon = True
        status_manager_queue_delayer.start()
        status_manager_thread.start()
        user_interrupt.start()
        done_writer_thread.start()
        log_writer_thread.start()

        log_writer_thread.log_output.put("Starting the run at "+str(datetime.datetime.now()))

        # Setting up geth "contract" variable
        code_to_send = 'var ABI = ' + curator_contract_ABI + ';\n'
        code_to_send += 'var contract_address = "' + curator_contract_address + '";\n'
        code_to_send += 'var '+contract_name+' = eth.contract(ABI).at(contract_address);\n'
        process.stdin.write(code_to_send)
        process.stdin.flush()

        # Setting up the event listening function
        code_to_send = 'var events = '+contract_name+'.allEvents();\n'
        code_to_send += 'events.watch(function(error, event){if (error) {console.log("Error: " + error);} else {console.log(event.event + ": " + JSON.stringify(event.args));}});;\n'
        process.stdin.write(code_to_send)
        process.stdin.flush()

        if mode == 'inserting' or mode == 'confirming' or mode == 'revoking':
            if args.start == 0 and args.end == 99999999999:
                working_range = xrange(len(targets_to_do))
            elif args.start == 0 and args.end != 99999999999:
                working_range = xrange(min(len(targets_to_do),args.end))
            elif args.start != 0 and args.end == 99999999999:
                working_range = xrange(args.start, len(targets_to_do))
            elif args.start != 0 and args.end != 99999999999:
                working_range = xrange(args.start, min(len(targets_to_do),args.end))

        # Now sending all transactions/confirmations/removing all transactions as requested.
            geth_timeouts = 0
            print ("Now starting to insert transactions. A dot will be displayed after each 20 transactions have been inserted. An x will be displayed every time geth is stuck for 5s. If that happens a lot the operation may take much longer.")
            if mode == 'inserting':
                check_column = 'TransactionSubmitted1'
            elif mode == 'confirming' and not args.final:
                check_column = 'TransactionSubmitted2'
            elif mode == 'confirming' and args.final:
                check_column = 'TransactionSubmitted3'
            for x in working_range:
                if targets_to_do.iloc[x].get_value(check_column) == 'No':
                    if mode == 'inserting':
                        funct = 'refund'
                    elif mode == 'confirming':
                        funct = 'confirm'
                    elif mode == 'revoking':
                        funct == 'revoke'
                    else:
                        raise SyntaxError("Mode wrong, it has to be one of inserting, confirming, revoking or listening.")
                    globals()[funct](process, single_row=targets_to_do.iloc[x:x + 1], work_list=status_manager_thread.waiting_for)
                    waiting_for_geth = True
                    while waiting_for_geth:
                        try:
                            line = all_geth_output_responses.get(timeout=5)
                            geth_timeouts = 0
                            waiting_for_geth = False
                        except:
                            geth_timeouts += 1
                            sys.stdout.write('x')
                            sys.stdout.flush()
                        if geth_timeouts > 5:
                            print ("Geth is stuck.. waiting 30s then trying to insert the next transaction, any transactions it misses will be retried later.")
                            time.sleep(30)
                            waiting_for_geth = False
                    if x % 20 == 0:
                        sys.stdout.write('.')
                        sys.stdout.flush()
                        time.sleep(1)  # to allow processing of already sent transactions
                        unlock_geth(process)
            print("Done, now waiting for replies from all transactions.")
        elif mode == "listening":
            print("This mode simply listens for transaction events and saves them.")

        print("Please wait until all threads are done.")

        status_manager_thread.noMoreNewTransactions = True
        if mode == 'listening':
            while not user_interrupt.interrupted:
                print ("Waiting for events. If debug is enabled they should be printed to the screen. Press q to exit (it may take 10s to exit).")
                time.sleep(10)
        else:
            last_transaction_re_check = int(calendar.timegm(time.gmtime()))
            last_transaction_re_insert = int(calendar.timegm(time.gmtime()))
            last_message_displayed = 0
            delta_events_in_this_re_checking_period = 0
            delta_events_in_last_re_checking_period = None
            last_delta_reset = int(calendar.timegm(time.gmtime()))
            while not status_manager_thread.allDone and not user_interrupt.interrupted:
                current_time = int(calendar.timegm(time.gmtime()))
                if current_time - last_message_displayed >= 10:
                    print("We have "+str(len(status_manager_thread.waiting_for))+" transactions we're waiting for. Sleeping for 10s before next update.(Press q to interrupt.)")
                    last_message_displayed = int(calendar.timegm(time.gmtime()))
                if current_time - last_delta_reset >= how_long_to_wait_before_re_checking_transactions:
                    delta_events_in_last_re_checking_period = delta_events_in_this_re_checking_period
                    delta_events_in_this_re_checking_period = 0
                    last_delta_reset = int(calendar.timegm(time.gmtime()))
                if current_time - last_transaction_re_check >= how_long_to_wait_before_re_checking_transactions:
                    recheck_transactions(process)
                    last_transaction_re_check = int(calendar.timegm(time.gmtime()))
                if delta_events_in_last_re_checking_period is not None and delta_events_in_last_re_checking_period == 0 and how_long_to_wait_before_re_inserting_transactions != 0 and current_time - last_transaction_re_insert >= how_long_to_wait_before_re_inserting_transactions:
                    re_insert_transactions(process)
                    last_transaction_re_insert = int(calendar.timegm(time.gmtime()))
                time.sleep(1)
            for i in targets_to_do.index:
                targets_to_do.set_value(i, 'extraBalanceAmount_new', "{0:.18f}".format((Decimal(long(targets_to_do.loc[i].get_value('extraBalanceAmount'))) / Decimal('1000000000000000000'))))
            targets_to_do['extraBalanceAmount'] = targets_to_do['extraBalanceAmount_new']
            targets_to_do.drop('extraBalanceAmount_new', axis=1, inplace=True)
            targets_to_do.to_csv(processed_targets_file_name, sep='\t')

    except Exception as e:
        traceback_template = '''Traceback (most recent call last):
            File "%(filename)s", line %(lineno)s, in %(name)s
            %(type)s: %(message)s\n'''
        print('Something failed. Have a look at the exception below:\n')
        print(e.message)
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback_details = {'filename': exc_traceback.tb_frame.f_code.co_filename, 'lineno': exc_traceback.tb_lineno, 'name': exc_traceback.tb_frame.f_code.co_name, 'type': exc_type.__name__, 'message': exc_value.message}
        del (exc_type, exc_value, exc_traceback)
        print
        print(traceback.format_exc())
        print
        print(traceback_template % traceback_details)
        print
        try:
            for i in targets_to_do.index:
                targets_to_do.set_value(i, 'extraBalanceAmount_new', "{0:.18f}".format((Decimal(long(targets_to_do.loc[i].get_value('extraBalanceAmount'))) / Decimal('1000000000000000000'))))
            targets_to_do['extraBalanceAmount'] = targets_to_do['extraBalanceAmount_new']
            targets_to_do.drop('extraBalanceAmount_new', axis=1, inplace=True)
            targets_to_do.to_csv(processed_targets_file_name, sep='\t')
            print ("Work done till this point was saved.")
        except:
            try:
                targets_to_do.to_csv(processed_targets_file_name, sep='\t')
                print ("Saved work already done in safe mode, the file is unusable until extraBalanceAmount is divided by 1000000000000000000, please use Excel or equivalent to modify. Also, tbe extraBalanceAmount_new column is to be removed.")
                print('Something failed. Have a look at the exception below:\n')
                print(e.message)
                exc_type, exc_value, exc_traceback = sys.exc_info()
                traceback_details = {'filename': exc_traceback.tb_frame.f_code.co_filename, 'lineno': exc_traceback.tb_lineno, 'name': exc_traceback.tb_frame.f_code.co_name, 'type': exc_type.__name__, 'message': exc_value.message}
                del (exc_type, exc_value, exc_traceback)
                print
                print(traceback.format_exc())
                print
                print(traceback_template % traceback_details)
                print
            except:
                print("Failed to save work done. Sorry, another exception was thrown.. perhaps the disk is full?")
                traceback_template = '''Traceback (most recent call last):
                            File "%(filename)s", line %(lineno)s, in %(name)s
                            %(type)s: %(message)s\n'''
                print('Something failed. Have a look at the exception below:\n')
                print(e.message)
                exc_type, exc_value, exc_traceback = sys.exc_info()
                traceback_details = {'filename': exc_traceback.tb_frame.f_code.co_filename, 'lineno': exc_traceback.tb_lineno, 'name': exc_traceback.tb_frame.f_code.co_name, 'type': exc_type.__name__, 'message': exc_value.message}
                del (exc_type, exc_value, exc_traceback)
                print
                print(traceback.format_exc())
                print
                print(traceback_template % traceback_details)
                print
        exit()


if __name__ == "__main__":
    main(sys.argv[1:])
